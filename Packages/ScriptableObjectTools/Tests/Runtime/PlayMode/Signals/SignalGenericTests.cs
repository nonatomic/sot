﻿using System.Collections;
using Nonatomic.SOT.Runtime.Signals;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Nonatomic.SOT.Tests.PlayMode.Signals
{
	public class SignalGenericTests
	{
		[UnityTest]
		public IEnumerator Signal_AddListener_Invokes()
		{
			var signal = new Signal<int>();
			var pass = false;

			void Callback(int value) => pass = true;
			signal.AddListener(Callback);
			signal.Invoke(3);

			yield return new WaitForEndOfFrame();

			Assert.True(pass);
		}

		[UnityTest]
		public IEnumerator Signal_RemoveListener_Invokes()
		{
			var signal = new Signal<int>();
			var pass = false;

			void Callback(int value) => pass = true;
			signal.AddListener(Callback);
			signal.RemoveListener(Callback);

			signal.Invoke(3);

			yield return new WaitForEndOfFrame();

			Assert.False(pass);
		}

		[UnityTest]
		public IEnumerator Signal_RemoveAllListeners_Invokes()
		{
			var signal = new Signal<int>();
			var pass = false;

			void Callback(int value) => pass = true;
			void CallbackTwo(int value) => pass = true;
			signal.AddListener(Callback);
			signal.AddListener(CallbackTwo);
			signal.RemoveAllListeners();

			signal.Invoke(3);

			yield return new WaitForEndOfFrame();

			Assert.False(pass);
		}

		[UnityTest]
		public IEnumerator Signal_Received_Int()
		{
			var signal = new Signal<int>();
			var result = 0;
			var testValue = 3;

			void Callback(int value) => result = value;
			signal.AddListener(Callback);
			signal.Invoke(testValue);

			yield return new WaitForEndOfFrame();

			Assert.AreEqual(result, testValue);
		}
	}
}