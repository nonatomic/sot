﻿using System.Collections;
using Nonatomic.SOT.Runtime.Signals;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Nonatomic.SOT.Tests.PlayMode.Signals
{
	public class SignalTests
	{
		[UnityTest]
		public IEnumerator Signal_AddListener_Invokes()
		{
			var signal = new Signal();
			var pass = false;

			void Callback() => pass = true;
			signal.AddListener(Callback);
			signal.Invoke();

			yield return new WaitForEndOfFrame();

			Assert.True(pass);
		}

		[UnityTest]
		public IEnumerator Signal_RemoveListener_Invokes()
		{
			var signal = new Signal();
			var pass = false;

			void Callback() => pass = true;
			signal.AddListener(Callback);
			signal.RemoveListener(Callback);

			signal.Invoke();

			yield return new WaitForEndOfFrame();

			Assert.False(pass);
		}

		[UnityTest]
		public IEnumerator Signal_RemoveAllListeners_Invokes()
		{
			var signal = new Signal();
			var pass = false;

			void Callback() => pass = true;
			void CallbackTwo() => pass = true;
			signal.AddListener(Callback);
			signal.AddListener(CallbackTwo);
			signal.RemoveAllListeners();

			signal.Invoke();

			yield return new WaitForEndOfFrame();

			Assert.False(pass);
		}
	}
}