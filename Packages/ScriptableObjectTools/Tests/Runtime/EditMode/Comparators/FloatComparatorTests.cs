﻿using Nonatomic.SOT.Runtime.Comparators;
using NUnit.Framework;

namespace Nonatomic.SOT.Tests.EditMode.Comparators
{
	public class FloatComparatorTests
	{
		[Test]
		public void Compare_EqualValues_ReturnsTrue()
		{
			var comparator = new FloatComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1.0f, 1.0f));
		}

		[Test]
		public void Compare_EqualValues_ToFourPlaces_ReturnsTrue()
		{
			var comparator = new FloatComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1.1248f, 1.1248f));
		}

		[Test]
		public void Compare_EqualValues_ToSixPlaces_ReturnsTrue()
		{
			var comparator = new FloatComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1.124835f, 1.124835f));
		}

		[Test]
		public void Compare_DifferentValues_ReturnsFalse()
		{
			var comparator = new FloatComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2.0f, 1.0f));
		}

		[Test]
		public void Compare_DifferentValues_ToFourPlaces_ReturnsFalse()
		{
			var comparator = new FloatComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2.1248f, 2.1249f));
		}

		[Test]
		public void Compare_DifferentValues_ToSixPlaces_ReturnsFalse()
		{
			var comparator = new FloatComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2.124835f, 2.124836f));
		}
	}
}