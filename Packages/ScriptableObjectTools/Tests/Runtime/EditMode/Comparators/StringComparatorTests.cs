﻿using Nonatomic.SOT.Runtime.Comparators;
using NUnit.Framework;

namespace Nonatomic.SOT.Tests.EditMode.Comparators
{
	public class StringComparatorTests
	{
		[Test]
		public void Compare_EqualValues_ReturnsTrue()
		{
			var comparator = new StringComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, "Value", "Value"));
		}

		[Test]
		public void Compare_DifferentValues_ReturnsFalse()
		{
			var comparator = new StringComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, "Value", "Valve"));
		}
	}
}