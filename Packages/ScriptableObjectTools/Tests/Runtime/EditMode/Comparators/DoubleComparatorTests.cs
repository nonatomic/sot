﻿using Nonatomic.SOT.Runtime.Comparators;
using NUnit.Framework;

namespace Nonatomic.SOT.Tests.EditMode.Comparators
{
	public class DoubleComparatorTests
	{
		[Test]
		public void Compare_EqualValues_ReturnsTrue()
		{
			var comparator = new DoubleComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1.0, 1.0));
		}

		[Test]
		public void Compare_EqualValues_ToFourPlaces_ReturnsTrue()
		{
			var comparator = new DoubleComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1.1248, 1.1248));
		}

		[Test]
		public void Compare_EqualValues_ToEightPlaces_ReturnsTrue()
		{
			var comparator = new DoubleComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1.12483579, 1.12483579));
		}

		[Test]
		public void Compare_DifferentValues_ReturnsFalse()
		{
			var comparator = new DoubleComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2.0, 1.0));
		}

		[Test]
		public void Compare_DifferentValues_ToFourPlaces_ReturnsFalse()
		{
			var comparator = new DoubleComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2.1248, 2.1249));
		}

		[Test]
		public void Compare_DifferentValues_ToEightPlaces_ReturnsFalse()
		{
			var comparator = new DoubleComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2.12483579, 2.12483578));
		}
	}
}