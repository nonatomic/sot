﻿using Nonatomic.SOT.Runtime.Comparators;
using NUnit.Framework;

namespace Nonatomic.SOT.Tests.EditMode.Comparators
{
	public class IntComparatorTests
	{
		[Test]
		public void Compare_EqualValues_ReturnsTrue()
		{
			var comparator = new IntComparator();
			Assert.True(comparator.Compare(ComparisonType.Equal, 1, 1));
		}

		[Test]
		public void Compare_DifferentValues_ReturnsFalse()
		{
			var comparator = new IntComparator();
			Assert.False(comparator.Compare(ComparisonType.Equal, 2, 1));
		}
	}
}