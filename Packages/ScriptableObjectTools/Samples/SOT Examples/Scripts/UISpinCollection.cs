using Nonatomic.SOT.Runtime.Lists;
using Nonatomic.SOT.Runtime.Variables.References;
using UnityEngine;

namespace Nonatomic.SOT.Samples.Examples.Scripts
{
	public class UISpinCollection : MonoBehaviour
	{
		[SerializeField] private SoRectTransformList instanceList;
		[SerializeField] private SoFloatReference spinForce;
		[SerializeField] private float friction = 10f;

		private float _deltaRotation;
		private float _minDeltaRotation = 0.01f;

		[ContextMenu("Spin")]
		public void Spin()
		{
			_deltaRotation += spinForce.Reference().Value;
		}

		private void Update()
		{
			ApplyRotationalInertia();
		}

		private void ApplyRotationalInertia()
		{
			if (Mathf.Abs(_deltaRotation) < _minDeltaRotation) return;

			foreach (var target in instanceList.Items)
			{
				target.Rotate (Vector3.back * Time.deltaTime, _deltaRotation);
			}
			
			_deltaRotation = Mathf.Lerp (_deltaRotation, 0, friction * Time.deltaTime);
		}
	}
}