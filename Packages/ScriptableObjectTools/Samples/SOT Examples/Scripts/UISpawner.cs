using Nonatomic.SOT.Runtime.Lists;
using UnityEngine;

namespace Nonatomic.SOT.Samples.Examples.Scripts
{
	public class UISpawner : MonoBehaviour
	{
		[SerializeField] private RectTransform prefab;
		[SerializeField] private SoRectTransformList instanceList;
		[SerializeField] private RectTransform spawnArea;
		[SerializeField] private int maxInstances;
		
		[ContextMenu("Spawn")]
		public void Spawn()
		{
			var instance = MakeInstance();
			instanceList.Add(instance);
		}

		private RectTransform MakeInstance()
		{
			var instance = GameObject.Instantiate(prefab, spawnArea);
			instance.anchoredPosition = RandomPositionWithin(spawnArea);
			return instance;
		}

		private Vector2 RandomPositionWithin(RectTransform transform)
		{
			var rect = transform.rect;
			var halfWidth = rect.width * 0.5f;
			var halfHeight = rect.height * 0.5f;

			float xPos = Random.Range(-halfWidth, halfWidth);
			float yPos = Random.Range(-halfHeight, halfHeight);
			
			return new Vector2(xPos, yPos);
		}
	}
}