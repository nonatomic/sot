using System;
using Nonatomic.SOT.Runtime.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace Nonatomic.SOT.Samples.Examples.Scripts
{
	[RequireComponent(typeof(Image))]
	public class UILight : MonoBehaviour
	{
		[SerializeField] private SoBoolVar lightValue;
		[SerializeField] private Color onColor;
		[SerializeField] private Color offColor;
		
		private Image _lightImage;

		private void Awake()
		{
			_lightImage = GetComponent<Image>();
			if (lightValue == null) throw new NullReferenceException("UILight is missing lightValue");
		}
		
		private void OnEnable()
		{
			lightValue.AddListener(OnLightSignal);
			OnLightSignal(lightValue.Value);
		}

		private void OnDisable()
		{
			lightValue.RemoveListener(OnLightSignal);
		}

		private void OnLightSignal(bool value)
		{
			(value ? (Action) TurnOn : TurnOff).Invoke();
		}

		private void TurnOn()
		{
			_lightImage.color = onColor;
		}

		private void TurnOff()
		{
			_lightImage.color = offColor;
		}
	}
}