using UnityEngine;

namespace Nonatomic.SOT.Samples.Examples.Scripts
{
	public class UISpin : MonoBehaviour
	{
		[SerializeField] private RectTransform target;
		[SerializeField] private float spinForce = 10f;
		[SerializeField] private float friction = 10f;

		private float _deltaRotation;
		private float _minDeltaRotation = 0.01f;

		[ContextMenu("Spin")]
		public void Spin()
		{
			_deltaRotation += spinForce;
		}

		private void Update()
		{
			ApplyRotationalInertia();
		}

		private void ApplyRotationalInertia()
		{
			if (Mathf.Abs(_deltaRotation) < _minDeltaRotation) return;

			target.Rotate (Vector3.back * Time.deltaTime, _deltaRotation);
			_deltaRotation = Mathf.Lerp (_deltaRotation, 0, friction * Time.deltaTime);
		}
	}
}
