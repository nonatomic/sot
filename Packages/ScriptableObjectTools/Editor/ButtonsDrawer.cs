using System.Collections.Generic;
using System.Reflection;
using Nonatomic.SOT.Runtime.Attributes;

namespace Nonatomic.SOT.Editor
{
	public class ButtonsDrawer
	{
		private const BindingFlags Flags = BindingFlags.Instance |
		                                   BindingFlags.Static |
		                                   BindingFlags.Public |
		                                   BindingFlags.NonPublic;

		private List<ButtonDrawer> _buttonDrawers = new List<ButtonDrawer>();

		public ButtonsDrawer(object target)
		{
			FindAllButtonAttributes(target);
		}

		private void FindAllButtonAttributes(object target)
		{
			var type = target.GetType();
			var methods = type.GetMethods(Flags);
			foreach (var method in methods)
			{
				var buttonAttribute = method.GetCustomAttribute<ButtonAttribute>();
				if (buttonAttribute == null) continue;

				_buttonDrawers.Add(new ButtonDrawer(method, buttonAttribute));
			}
		}

		public void DrawButtons(IEnumerable<object> targets)
		{
			foreach (ButtonDrawer button in _buttonDrawers)
			{
				button.Draw(targets);
			}
		}
	}
}