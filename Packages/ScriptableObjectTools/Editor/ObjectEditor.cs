using UnityEditor;
using UnityEngine;

namespace Nonatomic.SOT.Editor
{
	[CustomEditor(typeof(Object), true)]
	[CanEditMultipleObjects]
	public class ObjectEditor : UnityEditor.Editor
	{
		private ButtonsDrawer _buttonsDrawer;

		private void OnEnable()
		{
			_buttonsDrawer = new ButtonsDrawer(target);
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
			_buttonsDrawer.DrawButtons(targets);
		}
	}
}