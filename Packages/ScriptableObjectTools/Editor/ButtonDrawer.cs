using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Nonatomic.SOT.Runtime.Attributes;
using UnityEditor;
using UnityEngine;

namespace Nonatomic.SOT.Editor
{
	public class ButtonDrawer
	{
		private readonly string _displayName;
		private readonly MethodInfo _method;
		private readonly Parameter[] _params;

		public ButtonDrawer(MethodInfo method, ButtonAttribute attribute)
		{
			_method = method;
			_params = method.GetParameters().Select(info => new Parameter(info)).ToArray();
			_displayName = string.IsNullOrEmpty(attribute.Name)
				? ObjectNames.NicifyVariableName(method.Name)
				: attribute.Name;
		}

		public void Draw(IEnumerable<object> targets)
		{
			if (!GUILayout.Button(_displayName))
				return;

			foreach (object obj in targets)
			{
				//var paramValues = _params.Select<>(param => param.Value).ToArray();
				_method.Invoke(obj, null);
			}
		}
	}
}