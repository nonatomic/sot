﻿//warning CS4014: Because this call is not awaited, execution of the current method continues before the call is
//completed. Consider applying the 'await' operator to the result of the call.

#pragma warning disable 4014

using System;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace Nonatomic.SOT.Runtime.Persistence
{
	public abstract class SoJson : ScriptableObject
	{
		public bool autoLoad = true;
		public string url;

		public UnityEvent onLoadComplete = new UnityEvent();
		public bool IsLoaded => _loaded;

		private bool _loading;
		private bool _loaded;

		public void PopulateFromJson(string jsonData)
		{
			JsonUtility.FromJsonOverwrite(jsonData, (object)this);
		}

		protected virtual void OnEnable()
		{
			if (autoLoad) LoadFromUrl(url);
		}

		public async Task LoadFromUrl(string endPoint)
		{
			if (_loading || _loaded) return;

			_loading = true;

			using (var www = UnityWebRequest.Get(endPoint))
			{
				www.SendWebRequest();

				while (!www.isDone) await Task.Yield();

				JsonUtility.FromJsonOverwrite(www.downloadHandler.text, (object)this);
				_loading = false;
				_loaded = true;
				onLoadComplete.Invoke();
			}
		}

		public void LogJson(bool pretty = true)
		{
			Debug.Log($"JSON: {ToJson(pretty)}");
		}

		public void LogBase64JsonString(bool pretty = false)
		{
			Debug.Log($"BASE64: {Base64JsonString(pretty)}");
		}

		public string ToJson(bool pretty = true)
		{
			return JsonUtility.ToJson(this, pretty);
		}

		public string Base64JsonString(bool pretty = false)
		{
			var data = JsonUtility.ToJson(this, pretty);
			byte[] bytesToEncode = Encoding.UTF8.GetBytes(data);
			return Convert.ToBase64String(bytesToEncode);
		}
	}
}