﻿//warning CS4014: Because this call is not awaited, execution of the current method continues before the call is
//completed. Consider applying the 'await' operator to the result of the call.

#pragma warning disable 4014

using System.IO;
using System.Threading.Tasks;
using Nonatomic.SOT.Runtime.Attributes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace Nonatomic.SOT.Runtime.Persistence
{
	public abstract class SoStreaming : ScriptableObject
	{
		public bool autoLoad = true;
		public bool prettyJson = false;
		public UnityEvent onLoadComplete = new UnityEvent();

		public bool IsLoaded { get; private set; }

		public void PopulateFromJson(string jsonData)
		{
			JsonUtility.FromJsonOverwrite(jsonData, (object)this);
		}

		protected virtual void OnEnable()
		{
			if (autoLoad) LoadFromFile();
		}

		[Button]
		public async Task LoadFromFile()
		{
			var filePath = GetFilePath();

			using (var www = UnityWebRequest.Get(filePath))
			{
				www.SendWebRequest();

				while (!www.isDone) await Task.Yield();

				JsonUtility.FromJsonOverwrite(www.downloadHandler.text, (object)this);
				IsLoaded = true;
				onLoadComplete.Invoke();
			}
		}

		[Button]
		public void SaveToFile()
		{
			var json = JsonUtility.ToJson(this, prettyJson);
			var filePath = GetFilePath();
			StreamWriter writer = new StreamWriter(filePath, false);
			writer.Write(json);
			writer.Close();
		}

		public string GetFilePath()
		{
			var instanceName = name.Replace("(Clone)", string.Empty);
			return Path.Combine(UnityEngine.Application.streamingAssetsPath, $"{instanceName}.json");
		}
	}
}