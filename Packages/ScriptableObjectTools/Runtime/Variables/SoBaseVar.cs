using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nonatomic.SOT.Runtime.Signals;
using Nonatomic.SOT.Runtime.Variables.Constraint;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[Serializable]
	public abstract class SoBaseVar<T> : SoSignal<T>, IVar<T>
	{
		[SerializeField] private T value;
		[SerializeField] private T defaultValue;
		[SerializeField] private bool useDefault = true;
		[SerializeField] private List<Constraint<T>> constraints;

		public virtual T Value
		{
			get => value;
			set
			{
				//logger?.Log($"{this.name}.Value = {value}", this);
				this.value = value;
				Constrain();
				Invoke(this.value);
			}
		}

		private void Constrain()
		{
			if (constraints.Count == 0) return;
			foreach (var constraint in constraints) constraint.Execute(this);
		}

		public virtual void SetValueWithoutNotice(T value)
		{
			this.value = value;
		}

		public virtual void SetValue(T value)
		{
			//logger?.Log($"{this.name}.SetValue({value})", this);
			Value = value;
		}

		public virtual bool UseDefault
		{
			get => useDefault;
			set
			{
				useDefault = value;
				if (value) Value = DefaultValue;
			}
		}

		public virtual T DefaultValue
		{
			get => defaultValue;
			set
			{
				defaultValue = value;
				if (UseDefault) Value = value;
			}
		}

		public void Reset()
		{
			Value = UseDefault ? DefaultValue : Value;
		}

		protected virtual void OnEnable()
		{
			Reset();
			WaitThenNotify();
		}

		protected virtual async void WaitThenNotify()
		{
			await Task.Delay(1);
			Invoke(value);
		}
	}
}