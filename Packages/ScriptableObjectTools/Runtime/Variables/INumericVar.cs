namespace Nonatomic.SOT.Runtime.Variables
{
	public interface INumericVar<T>
	{
		void Add(T value);
		void Subtract(T value);
		void Multiply(T value);
		void Divide(T value);
	}
}