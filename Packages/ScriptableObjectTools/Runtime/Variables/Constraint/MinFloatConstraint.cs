using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables.Constraint
{
	[CreateAssetMenu(fileName = "MinFloatConstraint", menuName = "SOT/Constraints/MinFloatConstraint")]
	public class MinFloatConstraint : Constraint<float>
	{
		public float minValue;

		public override void Execute(SoBaseVar<float> baseVar)
		{
			if (baseVar.Value <= minValue) baseVar.SetValueWithoutNotice(minValue);
		}
	}
}