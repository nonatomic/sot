using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables.Constraint
{
	[CreateAssetMenu(fileName = "MinIntConstraint", menuName = "SOT/Constraints/MinIntConstraint")]
	public class MinIntConstraint : Constraint<int>
	{
		public int minValue;

		public override void Execute(SoBaseVar<int> baseVar)
		{
			if (baseVar.Value <= minValue) baseVar.SetValueWithoutNotice(minValue);
		}
	}
}