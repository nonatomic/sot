using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables.Constraint
{
	[CreateAssetMenu(fileName = "MaxIntConstraint", menuName = "SOT/Constraints/MaxIntConstraint")]
	public class MaxIntConstraint : Constraint<int>
	{
		public int maxValue;

		[ContextMenu("Execute")]
		public override void Execute(SoBaseVar<int> baseVar)
		{
			if (baseVar.Value >= maxValue) baseVar.SetValueWithoutNotice(maxValue);
		}
	}
}