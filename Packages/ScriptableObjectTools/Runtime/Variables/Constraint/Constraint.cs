using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables.Constraint
{
	public abstract class Constraint<T> : ScriptableObject
	{
		public abstract void Execute(SoBaseVar<T> baseVar);
	}
}