using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables.Constraint
{
	[CreateAssetMenu(fileName = "MinFloatConstraint", menuName = "SOT/Constraints/MaxFloatConstraint")]
	public class MaxFloatConstraint : Constraint<float>
	{
		public float maxValue;

		[ContextMenu("Execute")]
		public override void Execute(SoBaseVar<float> baseVar)
		{
			if (baseVar.Value >= maxValue) baseVar.SetValueWithoutNotice(maxValue);
		}
	}
}