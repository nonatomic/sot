using System;
using System.Threading.Tasks;
using Nonatomic.SOT.Runtime.Signals;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[Serializable]
	public class BaseVar<T> : Signal<T>, IVar<T>
	{
		[SerializeField] private T value;

		public virtual T Value
		{
			get => value;
			set
			{
				this.value = value;
				Invoke(this.value);
			}
		}

		public virtual void SetValueWithoutNotice(T value)
		{
			this.value = value;
		}

		public virtual void SetValue(T value)
		{
			Value = value;
		}

		public virtual void OnEnable()
		{
			WaitThenNotify();
		}

		protected virtual async void WaitThenNotify()
		{
			await Task.Delay(1);
			Invoke(value);
		}
	}
}