using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "FloatVar", menuName = "SOT/Variables/FloatVar")]
	public class SoFloatVar : SoBaseVar<float>, INumericVar<float>
	{
		public void Add(float value)
		{
			Value += value;
		}

		public void Subtract(float value)
		{
			Value -= value;
		}

		public void Multiply(float value)
		{
			Value *= value;
		}

		public void Divide(float value)
		{
			Value /= value;
		}
	}
}