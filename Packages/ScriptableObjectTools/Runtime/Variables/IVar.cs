namespace Nonatomic.SOT.Runtime.Variables
{
	public interface IVar<T>
	{
		void SetValueWithoutNotice(T value);
		void SetValue(T value);

		T Value { get; set; }
	}
}