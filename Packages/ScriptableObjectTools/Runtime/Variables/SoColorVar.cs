using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "ColorVar", menuName = "SOT/Variables/ColorVar")]
	public class SoColorVar : SoBaseVar<Color>
	{
	}
}