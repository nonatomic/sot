using System;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables.References
{
	[Serializable]
	public abstract class SoVarReference<T> : IVar<T>
	{
		[SerializeField] private SoBaseVar<T> soVar;
		[SerializeField] private BaseVar<T> constant;

		public IVar<T> Reference() => soVar == null ? constant : soVar;

		public T Value
		{
			get => Reference().Value;
			set => Reference().Value = value;
		}

		public virtual void SetValueWithoutNotice(T value)
		{
			Reference().SetValueWithoutNotice(value);
		}

		public void SetValue(T value)
		{
			Reference().SetValue(value);
		}
	}

	[Serializable]
	public class SoIntReference : SoVarReference<int>
	{
	}

	[Serializable]
	public class SoFloatReference : SoVarReference<float>
	{
	}

	[Serializable]
	public class SoBoolReference : SoVarReference<bool>
	{
	}

	[Serializable]
	public class SoVector2Reference : SoVarReference<Vector2>
	{
	}

	[Serializable]
	public class SoVector3Reference : SoVarReference<Vector3>
	{
	}
}