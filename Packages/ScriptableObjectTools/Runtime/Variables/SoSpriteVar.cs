using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "SpriteVar", menuName = "SOT/Variables/SpriteVar")]
	public class SoSpriteVar : SoBaseVar<Sprite>
	{
	}
}