using System;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[Serializable]
	[CreateAssetMenu(fileName = "StringVar", menuName = "SOT/Variables/StringVar")]
	public class SoStringVar : SoBaseVar<string>
	{
	}
}