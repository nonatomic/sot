using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "Vector3Var", menuName = "SOT/Variables/Vector3Var")]
	public class SoVector3Var : SoBaseVar<Vector3>
	{
	}
}