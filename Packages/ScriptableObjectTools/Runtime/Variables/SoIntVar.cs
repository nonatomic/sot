using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "IntVar", menuName = "SOT/Variables/IntVar")]
	public class SoIntVar : SoBaseVar<int>, INumericVar<int>
	{
		public void Add(int value)
		{
			Value += value;
		}

		public void Subtract(int value)
		{
			Value -= value;
		}

		public void Multiply(int value)
		{
			Value *= value;
		}

		public void Divide(int value)
		{
			Value /= value;
		}
	}
}