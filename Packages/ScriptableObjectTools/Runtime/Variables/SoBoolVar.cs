using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "BoolVar", menuName = "SOT/Variables/BoolVar")]
	public class SoBoolVar : SoBaseVar<bool>
	{
		public void Toggle()
		{
			Value = !Value;
		}

		public void ToggleWithoutNotify()
		{
			SetValueWithoutNotice(!Value);
		}
	}
}