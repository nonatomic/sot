using UnityEngine;

namespace Nonatomic.SOT.Runtime.Variables
{
	[CreateAssetMenu(fileName = "DoubleVar", menuName = "SOT/Variables/DoubleVar")]
	public class SoDoubleVar : SoBaseVar<double>, INumericVar<double>
	{
		public void Add(double value)
		{
			Value += value;
		}

		public void Subtract(double value)
		{
			Value -= value;
		}

		public void Multiply(double value)
		{
			Value *= value;
		}

		public void Divide(double value)
		{
			Value /= value;
		}
	}
}