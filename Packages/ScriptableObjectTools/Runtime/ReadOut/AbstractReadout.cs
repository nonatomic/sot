using Nonatomic.SOT.Runtime.Variables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Nonatomic.SOT.Runtime.ReadOut
{
	public abstract class AbstractReadOut<T> : MonoBehaviour
	{
		[SerializeField] protected SoBaseVar<T> variable;
		[SerializeField] protected string prefix;
		[SerializeField] protected string postfix;

		protected TMP_Text tmpLabel;
		protected Text textLabel;
		protected TextMesh textMesh;

		private void Awake()
		{
			tmpLabel = GetComponent<TMP_Text>();
			textLabel = GetComponent<Text>();
			textMesh = GetComponent<TextMesh>();
		}

		private void OnEnable()
		{
			variable.AddListener(OnValueChanged);
			OnValueChanged(variable.Value);
		}

		private void OnDisable()
		{
			variable.RemoveListener(OnValueChanged);
		}

		protected virtual void OnValueChanged(T value)
		{
		}
	}
}