using System;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.ReadOut
{
	[Serializable]
	public class BoolStringValue
	{
		public string trueValue;
		public string falseValue;

		public string GetStringValue(bool value)
		{
			return value ? trueValue : falseValue;
		}
	}

	public class BoolReadOut : AbstractReadOut<bool>
	{
		[SerializeField] private BoolStringValue boolStringValue;

		protected override void OnValueChanged(bool value)
		{
			var str = $"{prefix}{boolStringValue.GetStringValue(value)}{postfix}";
			if (tmpLabel != null) tmpLabel.text = str;
			if (textLabel != null) textLabel.text = str;
			if (textMesh != null) textMesh.text = str;
		}
	}
}