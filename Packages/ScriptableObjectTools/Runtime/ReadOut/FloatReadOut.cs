namespace Nonatomic.SOT.Runtime.ReadOut
{
	public class FloatReadOut : AbstractReadOut<float>
	{
		public string format;

		protected override void OnValueChanged(float value)
		{
			var str = $"{prefix}{value.ToString(format, null)}{postfix}";
			if (tmpLabel != null) tmpLabel.text = str;
			if (textLabel != null) textLabel.text = str;
			if (textMesh != null) textMesh.text = str;
		}
	}
}