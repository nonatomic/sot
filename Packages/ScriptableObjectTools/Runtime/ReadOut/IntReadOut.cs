namespace Nonatomic.SOT.Runtime.ReadOut
{
	public class IntReadOut : AbstractReadOut<int>
	{
		public string format;

		protected override void OnValueChanged(int value)
		{
			var str = $"{prefix}{value.ToString(format, null)}{postfix}";
			if (tmpLabel != null) tmpLabel.text = str;
			if (textLabel != null) textLabel.text = str;
			if (textMesh != null) textMesh.text = str;
		}
	}
}