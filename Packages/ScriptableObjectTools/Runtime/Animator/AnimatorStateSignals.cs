﻿// ReSharper disable Unity.NoNullPropagation

using System.Collections.Generic;
using Nonatomic.SOT.Runtime.Signals;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Animator
{
	public class AnimatorStateSignals : StateMachineBehaviour
	{
		public List<SoSignal> onStateEnter;
		public List<SoSignal> onStateExit;
		public List<SoSignal> onStateUpdate;
		public List<SoSignal> onStateMove;
		public List<SoSignal> onStateIK;

		public override void OnStateEnter(UnityEngine.Animator a, AnimatorStateInfo s, int l) => Execute(onStateEnter);
		public override void OnStateExit(UnityEngine.Animator a, AnimatorStateInfo s, int l) => Execute(onStateExit);

		public override void OnStateUpdate(UnityEngine.Animator a, AnimatorStateInfo s, int l) =>
			Execute(onStateUpdate);

		public override void OnStateMove(UnityEngine.Animator a, AnimatorStateInfo s, int l) => Execute(onStateMove);
		public override void OnStateIK(UnityEngine.Animator a, AnimatorStateInfo s, int l) => Execute(onStateIK);

		private void Execute(List<SoSignal> signalList)
		{
			if (signalList.Count == 0) return;
			signalList.ForEach(_ => _?.Invoke());
		}
	}
}