﻿using System;

namespace Nonatomic.SOT.Runtime.Comparators
{
	[Serializable]
	public abstract class Comparator<T>
	{
		public bool Compare(ComparisonType comparison, T argA, T argB)
		{
			return comparison switch
			{
				ComparisonType.Equal => IsEqual(argA, argB),
				ComparisonType.NotEqual => IsNotEqual(argA, argB),
				ComparisonType.LessThan => IsLessThan(argA, argB),
				ComparisonType.GreatThan => IsGreaterThan(argA, argB),
				ComparisonType.LessThanOrEqual => IsLessThanOrEqual(argA, argB),
				ComparisonType.GreaterThanOrEqual => IsGreaterThanOrEqual(argA, argB),
				_ => IsEqual(argA, argB)
			};
		}

		protected abstract bool IsEqual(T argA, T argB);
		protected abstract bool IsNotEqual(T argA, T argB);
		protected abstract bool IsLessThan(T argA, T argB);
		protected abstract bool IsGreaterThan(T argA, T argB);
		protected abstract bool IsLessThanOrEqual(T argA, T argB);
		protected abstract bool IsGreaterThanOrEqual(T argA, T argB);
	}
}