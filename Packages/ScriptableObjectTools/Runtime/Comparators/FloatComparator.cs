﻿namespace Nonatomic.SOT.Runtime.Comparators
{
	public class FloatComparator : Comparator<float>
	{
		protected override bool IsEqual(float argA, float argB) => argA == argB;
		protected override bool IsNotEqual(float argA, float argB) => argA != argB;
		protected override bool IsLessThan(float argA, float argB) => argA < argB;
		protected override bool IsGreaterThan(float argA, float argB) => argA > argB;
		protected override bool IsLessThanOrEqual(float argA, float argB) => argA <= argB;
		protected override bool IsGreaterThanOrEqual(float argA, float argB) => argA >= argB;
	}
}