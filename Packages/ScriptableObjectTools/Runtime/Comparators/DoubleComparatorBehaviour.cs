namespace Nonatomic.SOT.Runtime.Comparators
{
	public abstract class DoubleComparatorBehaviour : ComparatorBehaviour<double>
	{
		public void Awake()
		{
			comparator = new DoubleComparator();
		}
	}
}