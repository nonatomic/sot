﻿namespace Nonatomic.SOT.Runtime.Comparators
{
	public enum ComparisonType
	{
		Equal,
		NotEqual,
		LessThan,
		GreatThan,
		LessThanOrEqual,
		GreaterThanOrEqual
	}
}