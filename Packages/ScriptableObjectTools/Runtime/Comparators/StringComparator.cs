﻿namespace Nonatomic.SOT.Runtime.Comparators
{
	public class StringComparator : Comparator<string>
	{
		protected override bool IsEqual(string argA, string argB) => argA.Equals(argB);
		protected override bool IsNotEqual(string argA, string argB) => !argA.Equals(argB);
		protected override bool IsLessThan(string argA, string argB) => argA.Length < argB.Length;
		protected override bool IsGreaterThan(string argA, string argB) => argA.Length > argB.Length;
		protected override bool IsLessThanOrEqual(string argA, string argB) => argA.Length <= argB.Length;
		protected override bool IsGreaterThanOrEqual(string argA, string argB) => argA.Length >= argB.Length;
	}
}