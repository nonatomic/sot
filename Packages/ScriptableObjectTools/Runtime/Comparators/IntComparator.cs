﻿namespace Nonatomic.SOT.Runtime.Comparators
{
	public class IntComparator : Comparator<int>
	{
		protected override bool IsEqual(int argA, int argB) => argA == argB;
		protected override bool IsNotEqual(int argA, int argB) => argA != argB;
		protected override bool IsLessThan(int argA, int argB) => argA < argB;
		protected override bool IsGreaterThan(int argA, int argB) => argA > argB;
		protected override bool IsLessThanOrEqual(int argA, int argB) => argA <= argB;
		protected override bool IsGreaterThanOrEqual(int argA, int argB) => argA >= argB;
	}
}