using Nonatomic.SOT.Runtime.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace Nonatomic.SOT.Runtime.Comparators
{
	public abstract class ComparatorBehaviour<T> : MonoBehaviour
	{
		public SoBaseVar<T> valueA;
		public SoBaseVar<T> valueB;
		public ComparisonType comparisonType;
		public UnityEvent onTrue;
		public UnityEvent onFalse;

		protected Comparator<T> comparator;

		public void Compare()
		{
			(comparator.Compare(comparisonType, valueA.Value, valueB.Value) ? onTrue : onFalse).Invoke();
		}
	}
}