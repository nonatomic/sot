﻿namespace Nonatomic.SOT.Runtime.Comparators
{
	public class DoubleComparator : Comparator<double>
	{
		protected override bool IsEqual(double argA, double argB) => argA == argB;
		protected override bool IsNotEqual(double argA, double argB) => argA != argB;
		protected override bool IsLessThan(double argA, double argB) => argA < argB;
		protected override bool IsGreaterThan(double argA, double argB) => argA > argB;
		protected override bool IsLessThanOrEqual(double argA, double argB) => argA <= argB;
		protected override bool IsGreaterThanOrEqual(double argA, double argB) => argA >= argB;
	}
}