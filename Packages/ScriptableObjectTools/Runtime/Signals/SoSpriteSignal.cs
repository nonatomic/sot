using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoSpriteSignal", menuName = "SOT/Signals/SpriteSignal")]
	public class SoSpriteSignal : SoSignal<Sprite>
	{
	}
}