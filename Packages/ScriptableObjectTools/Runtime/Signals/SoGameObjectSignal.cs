using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoGameObjectSignal", menuName = "SOT/Signals/GameObjectSignal")]
	public class SoGameObjectSignal : SoSignal<GameObject>
	{
	}
}