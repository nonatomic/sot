using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoFloatSignal", menuName = "SOT/Signals/FloatSignal")]
	public class SoFloatSignal : SoSignal<float>
	{
	}
}