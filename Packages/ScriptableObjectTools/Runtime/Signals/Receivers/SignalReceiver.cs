﻿using System.Collections.Generic;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals.Receivers
{
	public class SignalReceiver : MonoBehaviour
	{
		public List<SignalMap> signals;

		private void OnEnable()
		{
			signals.ForEach(_ => _?.Enable());
		}

		private void OnDisable()
		{
			signals.ForEach(_ => _?.Disable());
		}

		public void InvokeAll()
		{
			signals.ForEach(_ => _?.Invoke());
		}
	}

	public class SignalReceiver<T> : MonoBehaviour
	{
		public List<SignalMap<T>> signals;

		private void OnEnable()
		{
			signals.ForEach(_ => _?.Enable());
		}

		private void OnDisable()
		{
			signals.ForEach(_ => _?.Disable());
		}

		public void InvokeAll(T value)
		{
			signals.ForEach(_ => _?.Invoke(value));
		}
	}
}