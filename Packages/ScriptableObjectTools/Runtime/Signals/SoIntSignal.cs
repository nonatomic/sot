using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoIntSignal", menuName = "SOT/Signals/IntSignal")]
	public class SoIntSignal : SoSignal<int>
	{
	}
}