using System;
using System.Collections.Generic;
using System.Linq;
using Nonatomic.SOT.Runtime.Attributes;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[Serializable]
	public class Signal : ISignal
	{
		protected List<Action> actions = new List<Action>();
		public bool Enabled { get; set; } = true;

		public virtual void AddListener(Action action)
		{
			actions.Add(action);
		}

		public virtual void RemoveListener(Action action)
		{
			actions.Remove(action);
		}

		public virtual void RemoveAllListeners()
		{
			actions.Clear();
		}

		[ContextMenu("Invoke")]
		[Button]
		public virtual void Invoke()
		{
			if (!Enabled) return;

			//note the use of .ToList() prevents the modification of the collection during invocation
			foreach (var action in actions.ToList())
			{
				action.Invoke();
			}
		}
	}

	[Serializable]
	public class Signal<T> : ISignal<T>
	{
		protected List<Action<T>> actions = new List<Action<T>>();
		public bool Enabled { get; set; } = true;

		public virtual void AddListener(Action<T> action)
		{
			actions.Add(action);
		}

		public virtual void RemoveListener(Action<T> action)
		{
			actions.Remove(action);
		}

		public virtual void RemoveAllListeners()
		{
			actions.Clear();
		}

		[ContextMenu("Invoke")]
		public virtual void Invoke(T value)
		{
			if (!Enabled) return;

			//note the use of .ToList() prevents the modification of the collection during invocation
			foreach (var action in actions.ToList())
			{
				action.Invoke(value);
			}
		}
	}
}