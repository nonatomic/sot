using System;

namespace Nonatomic.SOT.Runtime.Signals
{
	public interface ISignal
	{
		bool Enabled { get; set; }

		void AddListener(Action action);
		void RemoveListener(Action action);
		void RemoveAllListeners();
		void Invoke();
	}

	public interface ISignal<T>
	{
		bool Enabled { get; set; }

		void AddListener(Action<T> action);
		void RemoveListener(Action<T> action);
		void RemoveAllListeners();
		void Invoke(T value);
	}
}