using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Nonatomic.SOT.Runtime.Attributes;
using Nonatomic.SOT.Runtime.Logging;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Nonatomic.SOT.Runtime.Signals
{
	[Serializable]
	[CreateAssetMenu(fileName = "SoSignal", menuName = "SOT/Signals/Signal")]
	public class SoSignal : ScriptableObject, ISignal
	{
		public bool enabled = true;
		public SoLogger logger;

		protected List<Action> actions = new List<Action>();

		public bool Enabled { get; set; }

		public virtual void AddListener(Action action)
		{
			actions.Add(action);
		}

		public virtual void RemoveListener(Action action)
		{
			actions.Remove(action);
		}

		public virtual void RemoveAllListeners()
		{
			actions.Clear();
		}

		public virtual void RemoveAllListenersWithTarget(object target)
		{
			var actionsClone = actions.Where(action => action.Target.Equals(target)).ToArray();
			foreach (var action in actionsClone) actions.Remove(action);
		}
		
		public virtual void RemoveAllListenersWithTargetType(Type type)
		{
			var actionsClone = actions.Where(action => action.Target.GetType().FullName.Contains(type.FullName)).ToArray();
			foreach (var action in actionsClone) actions.Remove(action);
			
		}
		
		[ContextMenu("Invoke")]
		[Button]
		public virtual void Invoke()
		{
			if (!enabled) return;
			if (logger != null)
			{
				var stackTrace = new StackTrace();
				var frame = stackTrace.GetFrame(1);
				var method = frame.GetMethod();
				logger.Log($"{name} invoked by {method.ReflectedType?.Name}.{method.Name}");
			}

			//note the use of .ToList() prevents the modification of the collection during invocation
			foreach (var action in actions.ToList())
			{
				logger?.Log($"	- {action.Target}.{action.Method.Name}", this);
				action.Invoke();
			}
		}
	}

	[Serializable]
	public abstract class SoSignal<T> : ScriptableObject, ISignal<T>
	{
		public bool enabled = true;
		public SoLogger logger;
		protected List<Action<T>> actions = new List<Action<T>>();

		public bool Enabled { get; set; }

		public virtual void AddListener(Action<T> action)
		{
			actions.Add(action);
		}

		public virtual void RemoveListener(Action<T> action)
		{
			actions.Remove(action);
		}

		public virtual void RemoveAllListeners()
		{
			actions.Clear();
		}

		[ContextMenu("Invoke")]
		public virtual void Invoke(T value)
		{
			if (!enabled) return;
			if (logger != null)
			{
				var stackTrace = new StackTrace();
				var frame = stackTrace.GetFrame(1);
				var method = frame.GetMethod();
				logger.Log($"{name} invoked by {method?.ReflectedType?.Name}.{method.Name} with value {value}", this);
			}

			//note the use of .ToList() prevents the modification of the collection during invocation
			foreach (var action in actions.ToList())
			{
				logger?.Log($"	- {action.Target}.{action.Method.Name} with value {value}", this);
				action.Invoke(value);
			}
		}
	}
}