using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoStringSignal", menuName = "SOT/Signals/StringSignal")]
	public class SoStringSignal : SoSignal<string>
	{
	}
}