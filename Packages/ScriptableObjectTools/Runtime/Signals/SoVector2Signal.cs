﻿using Nonatomic.SOT.Runtime.Signals;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoVector2Signal", menuName = "SOT/Signals/Vector2Signal")]
	public class SoVector2Signal : SoSignal<Vector2>
	{
	}
}