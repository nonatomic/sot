﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Nonatomic.SOT.Runtime.Signals
{
	[Serializable]
	public class SignalMap
	{
		public List<SoSignal> signals;
		public UnityEvent onSignalReceived;

		public void Enable()
		{
			signals.ForEach(_ => _?.AddListener(Invoke));
		}

		public void Disable()
		{
			signals.ForEach(_ => _?.RemoveListener(Invoke));
		}

		public void Invoke()
		{
			onSignalReceived.Invoke();
		}
	}

	[Serializable]
	public class SignalMap<T>
	{
		public List<SoSignal<T>> signals;
		public UnityEvent<T> onSignalReceived;

		public void Enable()
		{
			Debug.Log($"SignalMap<{typeof(T)}> Enabled");
			signals.ForEach(_ => _?.AddListener(Invoke));
		}

		public void Disable()
		{
			Debug.Log($"SignalMap<{typeof(T)}> Disable");
			signals.ForEach(_ => _?.RemoveListener(Invoke));
		}

		public void Invoke(T value)
		{
			Debug.Log($"SignalMap<{typeof(T)}>.Invoke({onSignalReceived} with value:{value}");
			onSignalReceived.Invoke(value);
		}
	}
}