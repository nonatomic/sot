using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals
{
	[CreateAssetMenu(fileName = "SoBoolSignal", menuName = "SOT/Signals/BoolSignal")]
	public class SoBoolSignal : SoSignal<bool>
	{
	}
}