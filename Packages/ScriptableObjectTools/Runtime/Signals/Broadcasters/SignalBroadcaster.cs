﻿using System.Collections.Generic;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Signals.Broadcasters
{
	public class SignalBroadcaster : MonoBehaviour
	{
		public List<SoSignal> signals;

		public void Invoke()
		{
			signals.ForEach(_ => _?.Invoke());
		}
	}

	public class SignalBroadcaster<T> : MonoBehaviour
	{
		public List<SoSignal<T>> signals;
		public T value;

		public void Invoke()
		{
			signals.ForEach(_ => _?.Invoke(value));
		}

		public void InvokeWithValue(T newValue)
		{
			signals.ForEach(_ => _?.Invoke(newValue));
		}
	}
}