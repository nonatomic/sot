﻿#pragma warning disable 0219 //disable warning for not using color

using System.Collections.Generic;
using UnityEngine;
using static System.DateTime;
using Object = UnityEngine.Object;

namespace Nonatomic.SOT.Runtime.Logging
{
	public class Logger
	{
		private bool _enabled = true;
		private bool _includeTime = false;
		private Color32 _messageColor = Color.white;
		private Color32 _tagColor = Color.white;
		private string _tag = string.Empty;
		private List<string> _keyWordFilter = new List<string>();
		private List<string> _colorFilter = new List<string>();

		public void LogEnabled(bool enable) => _enabled = enable;
		public void SetMessageColor(Color32 color) => _messageColor = color;
		public void SetTagColor(Color32 color) => _tagColor = color;
		public void SetTag(string tag) => this._tag = tag;

		#region Filters

		public void LogColorFilter(string color) => _colorFilter = new List<string>() { color };
		public void AddLogColorFilter(string color) => _colorFilter.Add(color);
		public void LogColorFilter(List<string> colors) => _colorFilter = colors;
		public void LogKeyFilter(string keyword) => _keyWordFilter = new List<string>() { keyword };
		public void AddLogKeyFilter(string keyword) => _keyWordFilter.Add(keyword);
		public void LogKeyFilter(List<string> keywords) => _keyWordFilter = keywords;

		public void LogColorFilter(string[] colors)
		{
			_colorFilter = new List<string>();
			foreach (string color in colors)
				_colorFilter.Add(color);
		}

		public void LogKeyFilter(string[] keywords)
		{
			_keyWordFilter = new List<string>();
			foreach (var word in keywords)
				_keyWordFilter.Add(word);
		}

		private bool KeyFilterGuard(object msg)
		{
			if (_colorFilter.Count == 0 && _keyWordFilter.Count == 0)
				return false;

			foreach (string key in _keyWordFilter)
			{
				if (msg != null)
				{
					// Debug.Log(msg.ToString().Contains(key) + " = " + msg.ToString());
				}

				if (msg != null && msg.ToString().Contains(key))
				{
					return false;
				}
			}

			return true;
		}

		private bool ColorFilterGuard(string color)
		{
			if (_colorFilter.Count == 0 && _keyWordFilter.Count == 0)
				return false;

			if (_colorFilter.Contains(color))
			{
				return false;
			}

			return true;
		}

		#endregion

		#region Logging

		public void Log(string msg) => Log(msg, _messageColor);
		public void Log(object msg, Object target = null) => Log(msg, _messageColor, target);

		public void Log(object msg, Color32 color, Object target = null)
		{
			InternalLog(_tag, msg, ColorToHex(_tagColor), ColorToHex(color), target);
		}

		public void Log(object tag, object msg, Color32 tagColor, Color32 msgColor, Object target = null)
		{
			InternalLog(tag, msg, ColorToHex(tagColor), ColorToHex(msgColor), target);
		}

		public void LogWarning(object msg, Object target = null)
		{
			InternalWarningLog(msg, "Warning", "yellow", target);
		}

		public void LogError(object msg, Object target = null)
		{
			InternalWarningLog(msg, "Error", "red", target);
		}

		private void InternalLog(object tag, object msg, string tagColor, string msgColor, Object target = null)
		{
			if (!_enabled)
				return;

			if (KeyFilterGuard(msg) && KeyFilterGuard(tag) && ColorFilterGuard(tagColor) && ColorFilterGuard(msgColor))
				return;

#if !UNITY_EDITOR
				InternalFallbackLog(msg, tag, target);
				return;
#endif

			var tagStr = $"<b><color={tagColor}>[{tag}]</color>:</b> ";
			var msgStr = $"<color={msgColor}>{msg}<color>\n";
			Debug.Log($"{TimeString()}{tagStr}{msgStr}", target);
		}

		private void InternalWarningLog(object msg, string type, string color, Object target = null)
		{
#if !UNITY_EDITOR
				InternalFallbackLog(msg, type, target);
				return;
#endif

			Debug.Log($"{TimeString()}<color={color}>Error:{msg}</color>\n", target);
		}

		private void InternalFallbackLog(object msg, object type, Object target = null)
		{
			Debug.Log($"{TimeString()}{type}:{msg}", target);
		}

		private string TimeString()
		{
			if (!_includeTime) return string.Empty;

#if !UNITY_EDITOR
				return $"{System.DateTime.Now.ToString("mm:ss.ffff")} :: ";
#endif

			return $"<color=white>{Now:mm:ss.ffff}</color> :: ";
		}

		private string ColorToHex(Color32 color) => $"#{color.r:X2}{color.g:X2}{color.b:X2}";

		#endregion
	}
}