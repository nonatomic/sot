using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Nonatomic.SOT.Runtime.Logging
{
	[CreateAssetMenu(fileName = "Logger", menuName = "SOT/Logging/Logger")]
	public class SoLogger : ScriptableObject
	{
		[SerializeField] private bool enabled = true;
		[SerializeField] private bool includeTime = false;
		[SerializeField] private Color32 messageColor = Color.white;
		[SerializeField] private Color32 tagColor = Color.white;
		[SerializeField] private string tag = String.Empty;
		[SerializeField] private List<string> keyWordFilter = new List<string>();
		[SerializeField] private List<string> colorFilter = new List<string>();

		public void LogEnabled(bool enable)
		{
			enabled = enable;
		}

		public void SetMessageColor(Color32 color)
		{
			messageColor = color;
		}

		public void SetTagColor(Color32 color)
		{
			tagColor = color;
		}

		public void SetTag(string tag)
		{
			this.tag = tag;
		}

		#region Filters

		public void LogColorFilter(string color)
		{
			colorFilter = new List<string>() { color };
		}

		public void AddLogColorFilter(string color)
		{
			colorFilter.Add(color);
		}

		public void LogColorFilter(string[] colors)
		{
			colorFilter = new List<string>();
			foreach (string color in colors)
				colorFilter.Add(color);
		}

		public void LogColorFilter(List<string> colors)
		{
			colorFilter = colors;
		}

		public void LogKeyFilter(string keyword)
		{
			keyWordFilter = new List<string>() { keyword };
		}

		public void AddLogKeyFilter(string keyword)
		{
			keyWordFilter.Add(keyword);
		}

		public void LogKeyFilter(string[] keywords)
		{
			keyWordFilter = new List<string>();
			foreach (string word in keywords)
				keyWordFilter.Add(word);
		}

		public void LogKeyFilter(List<string> keywords)
		{
			keyWordFilter = keywords;
		}

		private bool KeyFilterGuard(object msg)
		{
			if (colorFilter.Count == 0 && keyWordFilter.Count == 0)
				return false;

			foreach (string key in keyWordFilter)
			{
				if (msg != null)
				{
					// Debug.Log(msg.ToString().Contains(key) + " = " + msg.ToString());
				}

				if (msg != null && msg.ToString().Contains(key))
				{
					return false;
				}
			}

			return true;
		}

		private bool ColorFilterGuard(string color)
		{
			if (colorFilter.Count == 0 && keyWordFilter.Count == 0)
				return false;

			if (colorFilter.Contains(color))
			{
				return false;
			}

			return true;
		}

		#endregion

		#region Logging

		/**
		 * Useful for inline logging from the UnityEvent Editor
		 */
		public void Log(string msg)
		{
			Log(msg, messageColor);
		}

		public void Log(object msg, Object target = null)
		{
			Log(msg, messageColor, target);
		}

		public void Log(object msg, Color32 color, Object target = null)
		{
			Log(tag, msg, ColorToHex(tagColor), ColorToHex(color), target);
		}

		public void Log(object tag, object msg, Color32 tagColor, Color32 msgColor, Object target = null)
		{
			Log(tag, msg, ColorToHex(tagColor), ColorToHex(msgColor), target);
		}

		public void Log(object tag, object msg, string tagColor, string msgColor, Object target = null)
		{
			if (!enabled)
				return;

			if (KeyFilterGuard(msg) && KeyFilterGuard(tag) && ColorFilterGuard(tagColor) && ColorFilterGuard(msgColor))
				return;

#if UNITY_EDITOR
			string tagStr = "<b><color=" + tagColor + ">[" + tag + "]</color>:</b> ";
			string msgStr = "<color=" + msgColor + ">" + msg + "</color>\n";
			Debug.Log(TimeString() + tagStr + msgStr, target);
#else
				Debug.Log(TimeString() + tag + ": " + msg, target );
#endif
		}

#pragma warning disable 0219 //disable warning for not using color
		public void LogWarning(object msg, Object target = null)
		{
			string color = "yellow";

#if UNITY_EDITOR
			Debug.Log(TimeString() + "<color=" + color + ">Warning:" + msg + "</color>\n", target);
#else
				Debug.Log(TimeString() + "Warning:" + msg, target);
#endif
		}

		public void LogError(object msg, Object target = null)
		{
			string color = "red";

#if UNITY_EDITOR
			Debug.Log(TimeString() + "<color=" + color + ">Error:" + msg + "</color>\n", target);
#else
				Debug.Log(TimeString() + "Error:" + msg, target);
#endif
		}

		#endregion

		#region Helpers

		private string TimeString()
		{
			if (!includeTime)
				return String.Empty;

#if UNITY_EDITOR
			return "<color=white>" + System.DateTime.Now.ToString("mm:ss.ffff") + "</color> :: ";
#else
				return System.DateTime.Now.ToString("mm:ss.ffff") + " :: ";
#endif
		}

		private string ColorToHex(Color32 color)
		{
			return "#" + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		}

		#endregion
	}
}