using System;

namespace Nonatomic.SOT.Runtime.Attributes
{
	[AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class ButtonAttribute : Attribute
	{
		/// <summary> Custom name of a button or <c>null</c> if not set. </summary>
		public readonly string Name;

		public ButtonAttribute()
		{
		}

		public ButtonAttribute(string name) => Name = name;
	}
}