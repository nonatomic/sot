﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace Nonatomic.SOT.Runtime.Extension
{
	public static class ScriptableObjectExtension
	{
		public static T Clone<T>(this T scriptableObject) where T : ScriptableObject
		{
			if (scriptableObject == null) return (T)ScriptableObject.CreateInstance(typeof(T));

			var instance = Object.Instantiate(scriptableObject);
			var instanceName = scriptableObject.name.Replace("(Clone)", string.Empty);
			instance.name = instanceName;
			return instance;
		}
	}
}