﻿using UnityEngine;

namespace Nonatomic.SOT.Runtime.Lists
{
	[CreateAssetMenu(fileName = "SoRectTransformList", menuName = "SOT/Lists/SoRectTransformList")]
	public class SoRectTransformList : SoRuntimeList<RectTransform>
	{
	}
}