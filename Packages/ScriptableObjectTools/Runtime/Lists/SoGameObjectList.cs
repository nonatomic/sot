﻿using UnityEngine;

namespace Nonatomic.SOT.Runtime.Lists
{
	[CreateAssetMenu(fileName = "SoGameObjectList", menuName = "SOT/Lists/SoGameObjectList")]
	public class SoGameObjectList : SoRuntimeList<GameObject>
	{
	}
}