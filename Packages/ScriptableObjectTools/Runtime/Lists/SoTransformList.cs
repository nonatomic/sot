﻿using UnityEngine;

namespace Nonatomic.SOT.Runtime.Lists
{
	[CreateAssetMenu(fileName = "SoTransformList", menuName = "SOT/Lists/SoTransformList")]
	public class SoTransformList : SoRuntimeList<Transform>
	{
	}
}