﻿using System.Collections.Generic;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Lists
{
	[CreateAssetMenu(fileName = "SoAudioSourceList", menuName = "SOT/Lists/SoAudioSourceList")]
	public class SoAudioSourceList : SoRuntimeList<AudioSource>
	{
		public AudioSource GetNextFree() => Items.Find(x => !x.isPlaying);
		public List<AudioSource> GetAllPlaying() => Items.FindAll(x => x.isPlaying);
		public List<AudioSource> GetAllFree() => Items.FindAll(x => !x.isPlaying);
	}
}