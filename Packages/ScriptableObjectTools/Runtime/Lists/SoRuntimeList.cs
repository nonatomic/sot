﻿using System.Collections.Generic;
using System.Linq;
using Nonatomic.SOT.Runtime.Variables;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Lists
{
	/**
	 * The list can store any objects including runtime scene objects
	 */
	public abstract class SoRuntimeList<T> : SoBaseVar<int>
	{
		[SerializeField] private bool allowDuplicates = false;
		[SerializeField] private bool clearAtRuntime = true;

		public List<T> Items { get; } = new List<T>();

		protected override void OnEnable()
		{
			if (clearAtRuntime) Clear();
		}

		public void Clear()
		{
			Items.Clear();
			Value = 0;
		}

		public void Add(T item)
		{
			if (allowDuplicates || !Items.Contains(item))
			{
				Items.Add(item);
				Value = Items.Count;
			}
		}

		public void Add(List<T> newItems)
		{
			foreach (var item in newItems) Add(item);
		}

		public void Remove(T item)
		{
			if (Items.Contains(item))
			{
				Items.Remove(item);
				Value = Items.Count;
			}
		}

		public T Pop()
		{
			var item = Items.Last();
			Items.RemoveAt(Items.Count - 1);
			Value = Items.Count;
			return item;
		}
	}
}