using Nonatomic.SOT.Runtime.Signals;
using Nonatomic.SOT.Runtime.Utils;
using UnityEngine;
#if UNITY_EDITOR
#endif

namespace Nonatomic.SOT.Runtime.Lists
{
	[CreateAssetMenu(fileName = "SoSignalList", menuName = "SOT/Lists/SoSignalList")]
	public class SoSignalList : SoAssetList<SoSignal>
	{
#if UNITY_EDITOR
		public new void OnEnable()
		{
			FindAllSignals();
		}

		[ContextMenu("FindAllSignals")]
		public void FindAllSignals()
		{
			base.Clear();
			var items = SoEditorUtils.GetAllScriptableObjects<SoSignal>();
			base.Add(items);
		}
#endif
	}
}