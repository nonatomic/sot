using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Nonatomic.SOT.Runtime.Utils
{
	public class SoEditorUtils
	{
#if UNITY_EDITOR
		public static List<T> GetAllScriptableObjects<T>() where T : ScriptableObject
		{
			var guids = AssetDatabase.FindAssets("t:" + typeof(T).Name);
			var instances = new List<T>();

			for (var i = 0; i < guids.Length; i++)
			{
				var path = AssetDatabase.GUIDToAssetPath(guids[i]);
				instances.Add(AssetDatabase.LoadAssetAtPath<T>(path));
			}

			return instances;
		}

		public static List<ScriptableObject> GetAllScriptableObjects()
		{
			var type = typeof(ScriptableObject);
			var guids = AssetDatabase.FindAssets("t:" + type.Name);
			var instances = new List<ScriptableObject>();

			for (var i = 0; i < guids.Length; i++)
			{
				var path = AssetDatabase.GUIDToAssetPath(guids[i]);
				instances.Add(AssetDatabase.LoadAssetAtPath<ScriptableObject>(path));
			}

			return instances;
		}
#endif
	}
}