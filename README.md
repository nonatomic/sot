# Scriptable Object Tools SOT #

Decoupling code in the editor. Empowering designers to wire up emergent behaviour.

## How do I get set up? ##

* Install as a UPM package in the editor
* Note as the package is a subfolder you need to add the path param and point to the subfolder when adding to Package Manager `{path to repo}?path=/Packages/ScriptableObjectTools`

## Who do I talk to? ##

* Repo owner paul@nonatomic.co.uk

## Core Concepts ###

### Inversion of Control ###
This project takes the view that the Unity Editor itself is a pretty good IoC container. Using the ```[SerializeField]``` attribute exposes an injection point without compromising on encapsulation.

### SoSignals ###
SoSignals are essentially events that exist as Scriptable Object assets. This allows you to drag n' drop references to them on Prefabs.

### SoVars ###
SoVars are data containers that exist as Scriptable Object assets. This allows you to drag n' drop references to them on Prefabs without introducing tight coupling.
The variables can also be subscribed to like events in order to bind data changes to outcomes.

### SoVarReference ###
Allows for any exposed variable to transition from a constant to a persisted or shared variable with ease.

### Read Outs ###
Simplify the process of binding data to screen readouts.